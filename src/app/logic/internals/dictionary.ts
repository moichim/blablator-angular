import { filter } from "rxjs";
import { DEFAULT_COMMAND, DictionaryCommand, DictionaryCommandBase, DictionaryCommandModes } from "./command";

// import { Markov } from "js-markov";
const Markov = require( "js-markov" );

const REMOVE_CHARACTERS = [
    "\n",
    "",
    "...",
    "_",
    "www",
];


export class Dictionary {

    public readonly markov: typeof Markov;
    public texts: string[] = [];
    protected isTrained: boolean = false;
    protected hasTexts: boolean = false;


    constructor( 
        public readonly id: string 
    ) {
        this.markov = new Markov();
    }

    addText(
        text: string,
        separator: string = "\n",
        toRemove?: string|string[]
    ): this {
        this.texts = this.texts.concat( this.processText( text, separator, toRemove ) );
        this.hasTexts = true;
        return this;
    }

    private processText(
        text: string,
        separator: string = "\n",
        toRemove?: string|string[]
    ): string[] {

        return text.split( separator ).map( paragraph => {

            let result = paragraph;
            
            // Build the replacables array
            const replaces = [...REMOVE_CHARACTERS];
            if ( toRemove && typeof toRemove === "string") {
                replaces.push( toRemove );
            } else if ( Array.isArray( toRemove ) ) {
                replaces.concat( toRemove );
            }

            // Perform the replacements
            replaces.forEach( pattern => {
                result = result.replace( pattern, "" );
            } );

            // Return the trimmed result
            return result.trim().replace( /  +/g, ' ' ) ;

        } ).filter( sentence => sentence !== "" );

    }

    train(
        nGram: number
    ): void {
        this.markov.clearChain();
        this.markov.addStates( this.texts );
        this.markov.train( nGram );
        this.isTrained = true;
    }

    generate( command?: DictionaryCommand ): string {

        let iterator = 5000;

        // Die if not trained
        if ( ! this.hasTexts ) throw new DictionaryException;

        // Train if not already
        if (!  this.isTrained ) this.train( 2 );

        // Set the commands as default if not present
        command = command ?? { ... DEFAULT_COMMAND };

        // Words are being generated simply
        if ( command.mode === DictionaryCommandModes.WORD ) {

            let result: string|false = false;

            while( result === false ) {

                if ( iterator <= 0 )
                    throw new Error( "Too many tries!" );

                result = Dictionary.lookForCommandedWord( this.markov.generateRandom( 500 ), command );

                iterator--;

            }

            return result;

        }

        // Generate sentence
        let result: string|false = false;

        while( result === false ) {

            if ( iterator <= 0 )
                throw new Error( "Too many tries!" );

            result = Dictionary.lookForCommandedSentence( this.markov.generateRandom( 500 ), command );


            iterator--;

        }

        return result;

        
    }


    // Single iteration word lookup
    static lookForCommandedSentence(
        text: string,
        command: DictionaryCommandBase
    ): string|false {

        let words = text.split( " " ).filter( word => word !== "" );

        const startOffset = 5 + Math.ceil( Math.random() * 5 );

        const maxLength = command.maxLength ?? 4 + ( Math.floor( Math.random() * 5 ) );

        // Exclude too short sentences
        
        if ( words.length <= 1 )
            return false;

        if ( command.minLength )
            if ( words.length < command.minLength )
                return false;

        if ( words.length < maxLength + startOffset )
            return false;

        // Return the subset
        return words
            .slice( startOffset,startOffset + maxLength )
            .join( " " );

    }


    // Single iteration word lookup
    static lookForCommandedWord(
        text: string,
        command: DictionaryCommandBase
    ): string|false {

        let startOnArray: string[] = command.startOn ?? [];
        let endOnArray: string[] = command.endOn ?? [];

        /* if ( command.startOn )
            if ( Array.isArray( command.startOn ) ) {
                startOnArray = command.startOn.length > 0 
                    ? command.startOn
                    : [];
            }
            else {
                startOnArray = command.startOn();
            }

        if ( command.endOn )
            if ( Array.isArray( command.endOn ) ) {
                endOnArray = command.endOn ?? [];
            }
            else {
                endOnArray = command.endOn();
            } */

        // Construct regexps once
        const startOn = Dictionary.stringsToRegExp( startOnArray, true );
        const endOn = Dictionary.stringsToRegExp( endOnArray, false );

        const filtered = text

            // Split by whitespaces
            .split( " " )

            // Remove all trailing characters
            .map( word => {
                return word.replace( /(\.||\,|\!|\?|\]|\[|\{|\}|\'|\`|\-|\_|\"|\(|\)|\%)/, "" );
            } )

            // Filter appropriate values
            .filter( word => {

                // Remove empty strings
                if ( word === "" ) return false;

                // Filter min length if necessary
                if ( command.minLength )
                    if ( word.length < command.minLength ) 
                        return false;

                // Filter max length if necessary
                if ( command.maxLength )
                    if ( word.length > command.maxLength ) 
                        return false;

                // Filter by start characters if necessary
                if ( command.startOn )
                    if ( word.match( startOn ) === null )
                        return false;

                // Filter by end characters if necessary
                if ( command.endOn )
                    if ( word.match( endOn ) === null )
                        return false;

                return true;
            } );

        if ( filtered.length > 0 ) {
            return filtered[ filtered.length - 1 ];
        }

        return false;
    }

    // Construct the regexp for word lookup
    static stringsToRegExp(
        strings: string[],
        starts: boolean
    ): RegExp
    {
        let pattern = "";
        
        if ( starts )
            pattern += "^";
        
        pattern += `(${strings.join("|")})`;

        if ( !starts )
            pattern += "$";

        return new RegExp( pattern, "gi" )
    }
}

export class DictionaryException extends Error {
}
