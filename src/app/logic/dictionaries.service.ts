import { isNgContent } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { Dictionary, DictionaryException } from './internals/dictionary';
import { getAnimalsDictionary, getArticleDictionary, getEnglishDictionary, getProphecyDictionary, getSimpleDictionary } from './internals/factories/dictionaries';

interface DictionariesIndex {
  [index:string]: Dictionary
}

@Injectable({
  providedIn: 'root'
})
export class DictionariesService {

  protected dictionaries: DictionariesIndex = {};

  constructor() {
    this.initialiseDictionaries();
  }

  addDictionary( item: Dictionary ) {
    if ( ! Object.keys( this.dictionaries ).includes( item.id ) ) {
      this.dictionaries[ item.id ] = item;
    } else {
      throw new DictionaryException( `Dictionary '${item.id}' already exists.` );
    }
  }

  getDictionaries(): DictionariesIndex
  {
    return this.dictionaries;
  }

  getDictionariesArray(): Dictionary[]
  {
    return Object.values( this.getDictionaries() );
  }

  getDictionary( key: string ): Dictionary
  {
    return this.dictionaries[ key ];
  }

  initialiseDictionaries(): void
  {
    this.addDictionary( getAnimalsDictionary() );
    this.addDictionary( getArticleDictionary() );
    this.addDictionary( getEnglishDictionary() );
    this.addDictionary( getProphecyDictionary() );
  }

}
