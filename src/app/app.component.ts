import { transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { ChildrenOutletContexts } from '@angular/router';
import { fadeInAnimation, standardAnimation } from './animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger("pageTransitions", [
      transition('intro <=> about', fadeInAnimation),
      transition('about <=> console', fadeInAnimation),
      transition('console <=> edit', standardAnimation),
    ]
    )
  ]
})
export class AppComponent {
  title = 'blbabla';

  constructor(private contexts: ChildrenOutletContexts) { }

  getRouteAnimationData() {
    return this.contexts.getContext('primary')?.route?.snapshot?.data?.['animation'];
  }

}

