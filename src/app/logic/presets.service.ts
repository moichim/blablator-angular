import { Injectable } from '@angular/core';
import { Preset } from './internals/preset';
import { getAnimalsPreset, getIntroductionPreset, getTestPreset } from "./internals/factories/presets";
import { DictionariesService } from './dictionaries.service';
import { DictionaryCommandModes } from './internals/command';
import { MessagesService } from './messages.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PresetsService {

  protected presets: Preset[] = [];

  public isSpeaking: boolean = false;

  public voices: SpeechSynthesisVoice[];

  constructor(
    public dictionariesService: DictionariesService,
    public messages: MessagesService,
    private router: Router
  ) {

    this.buildPresets();

    // Load the voice
    const synth = window.speechSynthesis;

    // For Safari & Mozilla, load it directly
    this.voices = synth.getVoices();

    // For Chrome, wait until languages are ready
    synth.addEventListener("voiceschanged", () => {
      this.voices = synth.getVoices();
    });

  }


  buildPresets() {
    this.presets.push(this.getIntroductionPreset());
    this.presets.push(this.getAnimalsPreset());
    this.presets.push(this.getInsultPreset());
    this.presets.push(this.getAdvicePreset());
    this.presets.push( this.getProphecyPreset() );

    Object.values( localStorage ).forEach( c => {

      const data = JSON.parse( c );

      const dictionary = this.dictionariesService.getDictionary( data.dictionary.id );
      const preset = new Preset(
        data.key,
        data.name,
        data.description,
        dictionary,
        data.command,
        data.languages,
        data.postprocess,
        true
      );

      this.presets.push( preset );

    } );

  }

  getPresets(): Preset[] {
    return this.presets;
  }

  getPreset(
    key: string
  ): Preset {
    return this.presets.find(p => p.key === key)!;
  }

  removePreset(
    preset:Preset
  ): void
  {
    this.messages.addMessage( `Generator ${preset.name} removed.` );
    this.presets = this.presets.filter( p => p.key !== preset.key );
    if ( Object.keys( localStorage ).includes( preset.key ) )
      localStorage.removeItem( preset.key );
  }

  addPreset(): Preset
  {

    const preset = new Preset(
      this.getFreeKey(),
      "Some new generator",
      "A new generator created by user.",
      this.dictionariesService.getDictionary( "english" ),
      {
        mode: DictionaryCommandModes.WORD,
        minLength: 5,
        maxLength: 15,
        endOn: [ "ing", "ent", "ur" ]
      },
      [ "cs_CZ" ],
      {},
      true
    );

    this.presets.push( preset );
    this.messages.addMessage( "Created a brand new generator!" );

    this.persistPreset( preset );

    setTimeout( () => this.router.navigate( [`/console/edit/${preset.key}`] ), 1 );

    return preset;
  }

  persistPreset(
    preset: Preset
  ): void
  {
    localStorage.setItem( preset.key, JSON.stringify( preset ) );
    this.messages.addMessage( `Generator ${preset.name} saved in local storage.` );
  }

  getKeys(): string[] {
    return this.presets.map(p => p.key);
  }

  getFreeKey(): string
  {

    const pick = () => {
      return available[ Math.ceil( Math.random() * available.length ) ];
    }

    const available = "abcdefghijklmnopqrstuvwxyz";
    let result = pick();

    while ( this.getKeys().includes( result ) ) {
      result = pick();
    }

    return result;
  }

  startSpeaking(): void {
    this.isSpeaking = true;
  }

  endSpeaking(): void {
    this.isSpeaking = false;
  }

  do(preset: Preset): void {

    if (this.isSpeaking === true)
      return;

    try {

      const result = preset.generate();

      let utterance = new SpeechSynthesisUtterance(result);

      utterance.voice = preset.getVoice();

      this.isSpeaking = true;

      speechSynthesis.speak(utterance);

      this.messages.addMessage(result);

      // Listen for speech end
      utterance.addEventListener("end", () => this.endUtterance());

    } catch (e) {
      this.messages.addMessage("Failed to generate new content. Too many tries!");
      console.error(e);
    }
  }


  async getLanguages(): Promise<SpeechSynthesisVoice[]> {
    const synth = window.speechSynthesis;
    return await synth.getVoices();
  }



  private endUtterance() {
    this.isSpeaking = false;
    this.messages.addMessage();
  }

  private getIntroductionPreset(): Preset {

    const preset = new Preset(
      "h",
      "Who am I?",
      "The robot shall introduce itself in English.",
      this.dictionariesService.getDictionary("english"),
      {
        mode: DictionaryCommandModes.WORD,
        minLength: 6,
        maxLength: 15,
        endOn: ["th", "en", "ak", "on", "er"]
      },
      ["en-US"]
    );

    preset.setPosprocessMask("Good day, my name is Mr. [result]");
    preset.setPosprocessUpperFirst(true);

    return preset;

  }

  private getAnimalsPreset(): Preset {
    return new Preset(
      "a",
      "New animal specie",
      "Robot will generate a new animal specie name in Czech.",
      this.dictionariesService.getDictionary("animals"),
      {
        mode: DictionaryCommandModes.SENTENCE,
        minLength: 2,
        maxLength: 2,
      },
      ["cs-CZ"]
    );
  }

  private getInsultPreset(): Preset {
    const preset = new Preset(
      "o",
      "Insult",
      "Robot will generate a new Czech insult.",
      this.dictionariesService.getDictionary("czech"),
      {
        mode: DictionaryCommandModes.WORD,
        minLength: 8,
        maxLength: 12,
        endOn: [ "le","če","ku","te","ávo","so" ]
      },
      ["cs-CZ"]
    );

    preset.setPosprocessMask( "Ty [result]!" );

    return preset;
  }

  private getAdvicePreset(): Preset {
    const preset = new Preset(
      "c",
      "Advice",
      "Robot will generate an advice in Czech.",
      this.dictionariesService.getDictionary("czech"),
      {
        mode: DictionaryCommandModes.WORD,
        minLength: 8,
        maxLength: 12,
        endOn: [ "ovat", "it" ]
      },
      ["cs-CZ"]
    );

    preset.setPosprocessMask( "Musíš víc [result]!" );

    return preset;
  }

  private getProphecyPreset(): Preset {
    const preset = new Preset(
      "p",
      "Prophecy",
      "Robot will produce a prophecy in Czech.",
      this.dictionariesService.getDictionary("prophecy"),
      {
        mode: DictionaryCommandModes.SENTENCE,
        minLength: 8,
        maxLength: 12,
      },
      ["cs-CZ"]
    );

    preset
      .setPosprocessMask( "Prorok pravil: '[result]!'" )
      .setPosprocessUpperFirst( true );

    return preset;
  }


}
