import { animals } from '../data/animals';
import { article } from '../data/article';
import { english } from '../data/english';
import { prophecy } from '../data/prophecy';
import { Dictionary } from '../dictionary';


export const getSimpleDictionary = () => (new Dictionary("simple")).addText("Something has just happened wtf co to bylo?\n Something awful has just happened.    \n\n\nA něco zase jinačího se stalo tady a tady.", ".", "wtf");

export const getAnimalsDictionary = () => (new Dictionary("animals")).addText(animals);

export const getArticleDictionary = () => (new Dictionary("czech")).addText(article);

export const getEnglishDictionary = () => (new Dictionary("english")).addText(english);

export const getProphecyDictionary = () => (new Dictionary("prophecy")).addText(prophecy);

export const getEmptyDictionary = () => new Dictionary("empty");
