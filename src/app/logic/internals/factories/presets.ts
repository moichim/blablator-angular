import { DictionaryCommandModes } from '../command';
import { getAnimalsDictionary, getArticleDictionary } from "./dictionaries";
import { Preset } from '../preset';


export const getTestPreset = () => {
  return new Preset(
    "i",
    "testovací preset",
    "Robot se mile představí",
    getArticleDictionary(),
    {
      mode: DictionaryCommandModes.WORD,
      minLength: 6,
      maxLength: 15,
      endOn: ["ová", "á", "tá", "ná"]
    }
  );
};

export const getIntroductionPreset = () => {
  const preset = new Preset(
    "k",
    "Kdo jsem",
    "HEzky se představte",
    getArticleDictionary(),
    {
      mode: DictionaryCommandModes.WORD,
      minLength: 6,
      maxLength: 15,
      endOn: ["ová", "á", "tá", "ná"]
    }
  );
  preset.setPosprocessMask( "Dobrý den, jsem nějaká [vysledek]" );

  return preset;
};

export const getAnimalsPreset = () => {
  return new Preset(
    "i",
    "Nové zvíře",
    "Robot se mile představí",
    getAnimalsDictionary(),
    {
      mode: DictionaryCommandModes.SENTENCE,
      minLength: 2,
      maxLength: 2,
    }
  );
};
