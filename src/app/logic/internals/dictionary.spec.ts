import { DEFAULT_COMMAND } from './command';
import { Dictionary, DictionaryException } from './dictionary';
import { getSimpleDictionary, getAnimalsDictionary, getEmptyDictionary } from './factories/dictionaries';

describe('Dictionary instance', () => {

  it('should create train and generate correctly', () => {

    // Instance creation
    const simple = getSimpleDictionary();
    expect( simple ).toBeTruthy();
    expect( simple.texts.length ).toBeGreaterThan( 0 );

    // Instance training
    const anim = getAnimalsDictionary();
    expect( anim.texts.length ).toBeGreaterThan( 0 );
    expect( () => anim.train(4) ).not.toThrowError();
    expect( () => anim.train(8) ).not.toThrowError();

    // Text generation
    
    // An already trained dictionary should generate correctly
    anim.train( 2 );
    expect( () => anim.generate() ).not.toThrowError();

    // An untrained dictionary should generate as well
    expect( () => getAnimalsDictionary().generate() ).not.toThrowError();
  
    // An untrained dictionary who does not have any texts should throw an error
    expect( () => getEmptyDictionary().generate() ).toThrow( new DictionaryException);

  });

});

describe("Dictionary helpers", () => {

  it("should convert correctly strings to regexps", () => {

    const startRegexp = Dictionary.stringsToRegExp(["an", "el", "ul"], true);

    expect("Anežka".match(startRegexp)).not.toBeNull();
    expect("Elvíra".match(startRegexp)).not.toBeNull();
    expect("Uličník".match(startRegexp)).not.toBeNull();
    expect("Lump".match(startRegexp)).toBeNull();
    expect("Lumpan".match(startRegexp)).toBeNull();

    const endRegexp = Dictionary.stringsToRegExp(["an", "el", "ul"], false);

    expect("Silmarian".match(endRegexp)).not.toBeNull();
    expect("Kundratovul".match(endRegexp)).not.toBeNull();
    expect("el".match(endRegexp)).not.toBeNull();
    expect("Ane".match(endRegexp)).toBeNull();
    expect("Lumpaň".match(endRegexp)).toBeNull();

  });

  it("should correctly identify generated words", () => {

    // Empty strings
    expect(Dictionary.lookForCommandedWord("", DEFAULT_COMMAND)).toBeFalse();

    // Min & Max

    // Min length is not passed
    expect(Dictionary.lookForCommandedWord("an ež ka", { minLength: 3 })).toBeFalse();

    // Min length is passes
    expect(Dictionary.lookForCommandedWord("an ež ka", { minLength: 2 })).not.toBeFalse();

    expect(Dictionary.lookForCommandedWord("anež káme i u", { minLength: 3 })).not.toBeFalse();

    // Max length is passes
    expect(Dictionary.lookForCommandedWord("anež káme ifsdfs sdfsd", { maxLength: 3 })).toBeFalse();

    // Max length is not passed
    expect(Dictionary.lookForCommandedWord("an ež ka", { maxLength: 4 })).not.toBeFalse();

    // Starts and ends

    // Starts on not passed
    expect( Dictionary.lookForCommandedWord( "Běžka", {startOn:["an","ux"]} ) ).toBeFalse();

    // Starts on passed
    expect( Dictionary.lookForCommandedWord( "Běžka", {startOn:["an","bě"]} ) ).not.toBeFalse();

    // Ends on not passed
    expect( Dictionary.lookForCommandedWord( "Běžka", {endOn:["an","ux"]} ) ).toBeFalse();

    // Ends on passed
    expect( Dictionary.lookForCommandedWord( "Běžka", {endOn:["an","ka"]} ) ).not.toBeFalse();

  });

  it( "should correctly identify generated sentences", () => {
    
    // Too short sentences
    expect( Dictionary.lookForCommandedSentence( "Aby tu byla", {} ) ).toBeFalse();

    // Too short sentences
    expect( Dictionary.lookForCommandedSentence( "Aby tu byla nějaká věta Aby tu byla nějaká věta Aby tu byla nějaká věta Aby tu byla nějaká věta Aby tu byla nějaká věta Aby tu byla nějaká věta Aby tu byla nějaká věta", {} ) ).not.toBeFalse();

    // Min length not passed
    expect( Dictionary.lookForCommandedSentence( "Ale nějaká krátká věta je tu", { minLength: 20 } ) ).toBeFalse();

    // Min length passed
    expect( Dictionary.lookForCommandedSentence( "Ale nějaká krátká věta je tu Ale nějaká krátká věta je tu Ale nějaká krátká věta je tuAle nějaká krátká věta je tuAle nějaká krátká věta je tu Ale nějaká krátká věta je tu", { minLength: 10 } ) ).not.toBeFalse();

    // Max length not passed

    expect( Dictionary.lookForCommandedSentence( "Ale nějaká věta tady je ale nechci aby tu byla a one tu potvora pořád je", { maxLength: 30 } ) ).toBeFalse();

    expect( Dictionary.lookForCommandedSentence( "Ale nějaká věta tady je ale nechci aby tu byla a one, tu potvora, pořád je a je tu a je tu a pořád mne štve.", { maxLength: 10 } ) ).not.toBeFalse();


  } );

});
