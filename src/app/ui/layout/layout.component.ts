import { Component, OnInit } from '@angular/core';
import { MessagesService } from 'src/app/logic/messages.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { trigger, transition } from '@angular/animations';
import { fadeInAnimation, standardAnimation, slideOutAnimation, slideInAnimation, verticalAnimation } from 'src/app/animations';
import { ChildrenOutletContexts } from '@angular/router';
import { PresetsService } from 'src/app/logic/presets.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  animations: [
    trigger("nestedTransitions", [
      transition('bar => edit', slideOutAnimation),
      transition('edit => bar', slideInAnimation),
      transition('* => info', slideInAnimation),
      transition('info => *', slideOutAnimation),
    ]
    )
  ]
})
export class LayoutComponent implements OnInit {

  private timeout?: NodeJS.Timeout;

  public youSee: boolean = true;

  constructor(
    public presets: PresetsService,
    public messages: MessagesService,
    public _snack: MatSnackBar,
    private contexts: ChildrenOutletContexts
  ) {

  }

  toggleVision(): void
  {
    this.youSee = !this.youSee;
  }

  ngOnInit(): void {
    this.messages.currentMessage.subscribe( incoming => {

      if ( incoming ) {
        this.openSnack( incoming );
      } else {
        this.closeSnack();
      }
      
    } );
  }

  private openSnack( text: string ): void
  {

    if ( this.timeout )
      this.closeSnack();

    this._snack.open( text );
    this.timeout = setTimeout( () => this.closeSnack(), 10000 );
  }

  private closeSnack(): void
  {
    this._snack.dismiss();
    if ( this.timeout ) clearTimeout( this.timeout );
  }

  getRouteAnimationData() {
    return this.contexts.getContext('primary')?.route?.snapshot?.data?.['animation'];
  }

}
