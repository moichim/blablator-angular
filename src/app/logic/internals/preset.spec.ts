import { DEFAULT_COMMAND } from './command';
import { getSimpleDictionary } from "./factories/dictionaries";
import { getTestPreset } from './factories/presets';

describe('Preset', () => {

  it('should create an instance', () => {

    // Instance creation
    const preset = getTestPreset();
    expect( preset ).toBeTruthy();

    // Simple generation
    for ( let i = 0; i<10; i++ ) {
      expect( preset.generate() ).not.toBeFalse();
    }

    // Upper first
    preset.setPosprocessUpperFirst( true );
    let result = preset.generate()[0];
    expect( result === result.toUpperCase() ).toBeTrue();

    // Lower first
    preset.setPosprocessLowerirst( true );
    result = preset.generate()[0];
    expect( result === result.toLowerCase() ).toBeTrue();

    // Mask without placeholder
    preset.setPosprocessMask( "Zvedavost" );
    expect( preset.generate() ).toBe( "Zvedavost" );

    // Mask with placeholder
    preset
      .setPosprocessMask( "Zvedavost [vysledek]" )
      .setPosprocessLowerirst( false );
    expect( preset.generate() ).toContain( "Zvedavost" );


  });

});
