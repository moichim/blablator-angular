import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Preset } from 'src/app/logic/internals/preset';
import { PresetsService } from 'src/app/logic/presets.service';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {

  public preset!: Preset;

  constructor(
    private route: ActivatedRoute,
    private presets: PresetsService
  ) { }

  ngOnInit(): void {
    const id = String( this.route.snapshot.paramMap.get( `id` ) );
    this.preset = this.presets.getPreset( id );
  }

}
