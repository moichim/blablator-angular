import { Component, HostListener, OnInit } from '@angular/core';
import { PresetsService } from 'src/app/logic/presets.service';

@Component({
  selector: 'app-console',
  templateUrl: './console.component.html',
  styleUrls: ['./console.component.scss']
})
export class ConsoleComponent implements OnInit {

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if( this.presetsService.getKeys().includes( event.key ) ){

      this.presetsService.do( this.presetsService.getPreset( event.key ) );

    }
  }

  constructor(
    public presetsService: PresetsService
  ) { }

  ngOnInit(): void {
  }

}
