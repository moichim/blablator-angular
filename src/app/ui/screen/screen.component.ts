import { animate, query, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/**
 * A single slideshow component defining title as input and content as children.
 * - needs to specify the next target and next button
 * - shall be instantiated in an individual component
 * - needs no further layout
 */
@Component({
  selector: 'app-screen',
  templateUrl: './screen.component.html',
  styleUrls: ['./screen.component.scss'],
  animations: [
      trigger( "progress", [
        transition(':enter', [
          style({ width: "0%" }),
          animate('5s', style({ width: "100%" })),
        ]),
      ] ),
    trigger( "screenTransitions", [
      transition( ":enter", [
        style({
          opacity: 0,
          transform: "scale( .5) translate(0 -5rem)"
        }),
        animate( "500ms", style( { 
          opacity: 1, 
          transform: "scale(1) translate(0rem 0rem)" 
        } ) )
      ] ),
      transition( ":leave", [
        style({
          opacity: 1,
          transform: "scale(1) translate(0)"
        }),
        animate( "500ms", style( { 
          opacity: 0, 
          transform: "scale(4) translate(5rem 0rem)" 
        } ) )
      ] ),
    ] )
  ]
})
export class ScreenComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() title!: string;
  @Input() nextLink!: string;
  @Input() nextText!: string;
  @Input() duration: number = 5000;
  protected timer!: NodeJS.Timeout;

  runs: boolean = false;

  constructor(
    protected router: Router
  ) { }

  ngOnInit(): void {

    this.timer = setTimeout( () => {
      this.onClick();
    }, this.duration );

  }

  ngOnDestroy(): void {
    if ( this.timer )
      clearTimeout( this.timer );
  }

  ngAfterViewInit(): void
  {
    this.runs = true;
  }

  onClick(): void
  {
    this.router.navigate( [this.nextLink] );
  }

}
