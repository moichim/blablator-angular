import { Component, Input } from '@angular/core';
import { Preset } from 'src/app/logic/internals/preset';
import { PresetsService } from 'src/app/logic/presets.service';

@Component({
  selector: 'app-trigger',
  templateUrl: './trigger.component.html',
  styleUrls: ['./trigger.component.scss']
})
export class TriggerComponent {

  @Input() preset!: Preset;

  constructor( 
      public presetService: PresetsService 
  ) {}

  onClick() {
    this.presetService.do( this.preset );
  }

  onRemove() {
    this.presetService.removePreset( this.preset );
  }

}
