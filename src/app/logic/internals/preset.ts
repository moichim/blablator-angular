import { DictionaryCommand, DictionaryCommandModes } from "./command";
import { Dictionary } from "./dictionary";

import {upperFirst, lowerFirst} from "lodash";

export interface PresetPostProcess {
    mask?: string|(() => string)
    upperFirst?: true,
    lowerFirst?: true,
    randomizer?: (preset:Preset) => Preset
}

const MASK_MARKER = "[result]";

export class Preset {

    public voice: SpeechSynthesisVoice;
    protected voices: SpeechSynthesisVoice[];

    constructor(
        public key: string,
        public name: string,
        public description: string,
        public dictionary: Dictionary,
        public command: DictionaryCommand,
        public languages: string[] = ["cs-CZ"],
        public postprocess: PresetPostProcess = {},
        public user: boolean = false
    ) {

        const synth = window.speechSynthesis;

        // Load the voice
        this.voices = synth.getVoices();
        
        // For Safari & Mozilla, load it directly
        this.voice = this.fetchVoiceFromLanguage( this.voices );

        // For Chrome, wait until languages are ready
        synth.addEventListener("voiceschanged", () => {
            this.voices = synth.getVoices();
            this.voice = this.fetchVoiceFromLanguage( this.voices );
        });

    }

    protected fetchVoiceFromLanguage( voices: SpeechSynthesisVoice[] ):SpeechSynthesisVoice {

        let result: SpeechSynthesisVoice|undefined = undefined;

        for ( let language of this.languages ) {

            if ( result === undefined ) {
                const matching = this.voices.filter( v => v.lang.includes( language ) );

                if ( matching.length > 0 ) {
                    result = matching[0];
                }
            }

        }

        return result ?? this.voices[ 0 ];

    }



    setPosprocessMask( mask:string|(()=>string) ): this
    {
        this.postprocess.mask = mask;
        return this;
    }

    setPosprocessUpperFirst( apply: boolean ): this
    {
        this.postprocess.upperFirst = apply === true 
            ? apply 
            : undefined;

        if ( apply === true )
            this.postprocess.lowerFirst = undefined;
        return this;
    }

    setPosprocessLowerirst( apply: boolean ): this
    {
        this.postprocess.lowerFirst = apply === true 
            ? apply 
            : undefined;

        if ( apply === true )
            this.postprocess.upperFirst = undefined;
        return this;
    }

    setRandomizer(
        randomizer: ( preset: Preset ) => this
    ): this
    {
        this.postprocess.randomizer = randomizer;
        return this;
    }

    getVoice(): SpeechSynthesisVoice
    { 
        return this.voice;
    }



    generate():string 
    {
        let result = this.dictionary.generate( this.command );


        // Apply only when postprocess is set

        if ( this.postprocess ) {

            // Apply upperfirst
            if ( this.postprocess.upperFirst )
                if ( this.postprocess.upperFirst === true )
                    result = upperFirst( result );

            // Apply lowerfirst
            if ( this.postprocess.lowerFirst )
                if ( this.postprocess.lowerFirst === true )
                    result = lowerFirst( result );
            
            // Apply output mask
            if ( this.postprocess.mask ) {

                const mask = typeof this.postprocess.mask === "string"
                    ? this.postprocess.mask
                    : this.postprocess.mask();

                if ( mask.includes( MASK_MARKER ) ) {
                    result = mask.replace( MASK_MARKER, result );
                } else {
                    result = mask;
                }
            }

            // Last, apply the rendomizer
            if ( this.postprocess.randomizer )
                this.postprocess.randomizer( this );

        }

        return result;
    }

    do() {
       
        const result = this.generate();

    }

    getMode(): string
    {
        return this.command.mode === DictionaryCommandModes.WORD
            ? "word"
            : "sentence";
    }

}
