import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { DictionaryCommandModes, DictionaryCommandSyllabs } from 'src/app/logic/internals/command';
import { Preset } from 'src/app/logic/internals/preset';

import {MatChipEvent, MatChipInputEvent} from '@angular/material/chips';
import {MatSelectChange} from "@angular/material/select";

import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { DictionariesService } from 'src/app/logic/dictionaries.service';
import { PresetsService } from 'src/app/logic/presets.service';
import { MessagesService } from 'src/app/logic/messages.service';
import * as _ from 'lodash';

interface Syllab {
  name: string
}

type SyllabField = "startOn"|"endOn";

interface SyllabList {
  field: SyllabField,
  internal: Syllab[],
  real: DictionaryCommandSyllabs
}

@Component({
  selector: 'app-preset-form',
  templateUrl: './preset-form.component.html',
  styleUrls: ['./preset-form.component.scss']
})
export class PresetFormComponent implements OnInit, OnDestroy {

  /** The preset shall be passed directly from template */
  @Input() public preset!: Preset;

  public startOn!: SyllabList;
  public endOn!: SyllabList;

  public readonly modes = DictionaryCommandModes;

  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  constructor(
    public dectionaries: DictionariesService,
    public presets: PresetsService,
    public messages: MessagesService
  ) { }

  ngOnInit(): void {
    this.startOn = this.parseArrayToChips( "startOn", this.preset.command.startOn );
    this.endOn = this.parseArrayToChips( "endOn", this.preset.command.endOn );
  }

  ngOnDestroy(): void {
    this.presets.persistPreset( this.preset );
  }

  private parseArrayToChips( 
    field: SyllabField,
    presetValue?: DictionaryCommandSyllabs 
  ): SyllabList
  {
    let chips: Syllab[] = [];
    let internal: string[] = [];

    if ( presetValue ) {
      internal = presetValue ?? [];
      chips = presetValue.map( val => this.strToChip( val )
      );
    }

    return {
      field: field,
      internal: chips,
      real: internal
    };

  }

  dictionarySelect( event: MatSelectChange ): void {
    this.preset.dictionary = event.value;
  }

  private strToChip = ( val: string ): Syllab => 
  ({ name: val })

  chipAdd(
    event: MatChipInputEvent,
    list: SyllabList
  ): void {

    let item = _.trim( event.value, "?:.!-_><[]()=+-\\\/\|~" );

    if (! list.real.includes( item ) && item.length > 0) {

      list.real.push( item );
      list.internal.push( this.strToChip( item ) );
      this.preset.command[ list.field ] = list.real;
    }

    event.chipInput!.clear();

  }

  chipRemove(
    value: Syllab,
    list: SyllabList,
    event: MatChipEvent
  ): void
  {

    list.internal = list.internal.filter( v => v.name !== value.name );
    list.real = list.real.filter( i => i !== value.name );
    this.preset.command[ list.field ] = list.real;

  }

  chipEdit(
    value: Syllab,
    list: SyllabList,
    // event: Event
  ): void
  {

  }

  dictionarySelected( event: MatSelectChange ): void
  {
    this.preset.dictionary = event.value;
  }

  modeSelected( event: MatSelectChange ): void
  {
    this.preset.command.mode = event.value;
  }

  voiceSelected( event: MatSelectChange ): void
  {
    this.preset.voice = event.value;
  }

  do() {

    this.presets.do( this.preset );

  }

}
