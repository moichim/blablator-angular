import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  private messageSource = new BehaviorSubject<string|undefined>(undefined);
  currentMessage = this.messageSource.asObservable();

  constructor() { }

  addMessage( text?: string ): void
  {
    this.messageSource.next( text );
  }

}
