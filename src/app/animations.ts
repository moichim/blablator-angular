import { animate, animateChild, group, query, style } from '@angular/animations';

export const fadeInAnimation = [
  style({ position: 'relative' }),
  query(':enter, :leave', [
    style({
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%'
    })
  ]),
  query(':enter', [
    style({ opacity: 0 })
  ]),
  query(':leave', animateChild()),
  group([
    query(':leave', [
      animate('.5s ease-out', style({ opacity: 0 }))
    ]),
    query(':enter', [
      animate('.5s ease-out', style({ opacity: 1 }))
    ]),
    query('@*', animateChild(), {optional: true })
  ]),
];


export const standardAnimation = [
  style({ position: 'relative' }),
  query(':enter, :leave', [
    style({
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%'
    })
  ]),
  query(':enter', [
    style({ left: '-100%' })
  ]),
  query(':leave', animateChild()),
  group([
    query(':leave', [
      animate('500ms ease-out', style({ left: '100%', opacity: 0 }))
    ]),
    query(':enter', [
      animate('500ms ease-out', style({ left: '0%' }))
    ]),
    query('@*', animateChild(), {optional: true })
  ]),
];

export const verticalAnimation = [
  style({ position: 'relative' }),
  query(':enter, :leave', [
    style({
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%'
    })
  ]),
  query(':enter', [
    style({ top: '-100%' })
  ]),
  query(':leave', animateChild()),
  group([
    query(':leave', [
      animate('500ms ease-out', style({ top: '100%', opacity: 0 }))
    ]),
    query(':enter', [
      animate('500ms ease-out', style({ top: '0%' }))
    ]),
    query('@*', animateChild(), {optional: true })
  ]),
];

export const slideOutAnimation = [
  style({ position: 'relative' }),
  query(':enter, :leave', [
    style({
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      overflow: "hidden"
    })
  ]),
  query(':enter', [
    style({ opacity: 0, transform: "scale(.2) translate(0px, -500px)" })
  ]),
  query(':leave', animateChild()),
  group([
    query(':leave', [
      animate('.5s ease-out', style({ opacity: 0, transform: "scale(2) translate(0px,500px)" }))
    ]),
    query(':enter', [
      animate('.5s ease-out', style({ opacity: 1, transform: "scale(1) translate(0px, 0px)", overflow: "auto" }))
    ]),
    query('@*', animateChild(), {optional: true })
  ]),
];

export const slideInAnimation = [
  style({ position: 'relative' }),
  query(':enter, :leave', [
    style({
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%'
    })
  ]),
  query(':enter', [
    style({ opacity: 0, transform: "scale(2) translate(0px, 500px)", overflow: "hidden" })
  ]),
  query(':leave', animateChild()),
  group([
    query(':leave', [
      animate('.5s ease-out', style({ opacity: 0, transform: "scale(.2) translate(0px,-500px)" }))
    ]),
    query(':enter', [
      animate('.5s ease-out', style({ opacity: 1, transform: "scale(1) translate(0px, 0px)", overflow: "auto" }))
    ]),
    query('@*', animateChild(), {optional: true })
  ]),
];