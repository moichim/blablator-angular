import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScreenOneComponent } from './ui/intro/screen-one/screen-one.component';
import { ScreenTwoComponent } from './ui/intro/screen-two/screen-two.component';
import { ConsoleComponent } from './ui/console/console.component';
import { EditorComponent } from './ui/editor/editor.component';
import { InfoComponent } from './ui/info/info.component';
import { LayoutComponent } from './ui/layout/layout.component';

const routes: Routes = [
  
  { 
    path: "", 
    redirectTo: "/intro", 
    pathMatch: "full",
  },

  // The intro sequence
  {
    path: "intro",
    component: ScreenOneComponent,
    data: {
      animation: "intro"
    }
  },
  {
    path: "about",
    component: ScreenTwoComponent,
    data: {
      animation: "about"
    }
  },
  
  // The main UI
  { 
    path: "console", 
    component: LayoutComponent,
    data: {
      animation: "console"
    },
    children: [
      {
        path: "",
        redirectTo: "bar", 
        pathMatch: "full",
      },
      { 
        path: "bar", 
        component: ConsoleComponent,
        data: {
          animation: "bar"
        },
      },
      {
        path: "edit/:id", 
        component: EditorComponent,
        data: {
          animation: "edit"
        }
      },
      {
        path: "info", 
        component: InfoComponent,
        data: {
          animation: "info"
        }
      },
    ]
  },
  { 
    path: "edit/:id", 
    component: EditorComponent,
    data: {
      animation: "edit"
    }
  },
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
