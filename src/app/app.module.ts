import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from "@angular/forms";

import { MatCardModule } from "@angular/material/card";
import { MatButtonModule } from "@angular/material/button";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatSidenavModule } from "@angular/material/sidenav"
import {MatListModule} from "@angular/material/list";
import {MatFormFieldModule } from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import { MatInputModule } from "@angular/material/input";
import { MatChipsModule } from "@angular/material/chips";
import { MatIconModule } from "@angular/material/icon";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatExpansionModule } from "@angular/material/expansion";


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConsoleComponent } from './ui/console/console.component';
import { TriggerComponent } from './ui/trigger/trigger.component';
import { NavigationComponent } from './ui/navigation/navigation.component';
import { LayoutComponent } from './ui/layout/layout.component';
import { PresetFormComponent } from './ui/preset-form/preset-form.component';
import { EditorComponent } from './ui/editor/editor.component';
import { DictionariesService } from './logic/dictionaries.service';
import { PresetsService } from './logic/presets.service';

import { ScreenComponent } from './ui/screen/screen.component';
import { ScreenOneComponent } from './ui/intro/screen-one/screen-one.component';
import { ScreenTwoComponent } from './ui/intro/screen-two/screen-two.component';
import { InfoComponent } from './ui/info/info.component';
import { WebcamComponent } from './ui/webcam/webcam.component'

@NgModule({
  declarations: [
    AppComponent,
    ConsoleComponent,
    TriggerComponent,
    NavigationComponent,
    LayoutComponent,
    PresetFormComponent,
    EditorComponent,
    ScreenComponent,
    ScreenOneComponent,
    ScreenTwoComponent,
    InfoComponent,
    WebcamComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    
    MatCardModule,
    MatButtonModule,
    MatGridListModule,
    MatSidenavModule,
    MatListModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatChipsModule,
    MatIconModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatExpansionModule

  ],
  providers: [
    DictionariesService,
    PresetsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
