export enum DictionaryCommandModes {
    WORD = 1,
    SENTENCE = 2
};

export type DictionaryCommandSyllabs = string[];

export interface DictionaryCommandBase {
    minLength?: number,
    maxLength?: number,
    endOn?: DictionaryCommandSyllabs,
    startOn?: DictionaryCommandSyllabs
}

export interface DictionaryCommand extends DictionaryCommandBase {
    mode: DictionaryCommandModes
}

export const DEFAULT_COMMAND: DictionaryCommand = {
    mode: DictionaryCommandModes.WORD
}
