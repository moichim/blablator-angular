import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessagesService } from 'src/app/logic/messages.service';
import { PresetsService } from 'src/app/logic/presets.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(
    protected presets: PresetsService,
    protected router: Router,
    private messages: MessagesService
  ) { }

  ngOnInit(): void {
  }

  onAdd() {
    this.presets.addPreset();
  }

  isCurrent( route: string ): string | undefined
  {
    return this.router.url === route ? "primary" : undefined;
  }

}
