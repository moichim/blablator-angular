import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import  * as p5 from "p5";

@Component({
  selector: 'app-webcam',
  templateUrl: './webcam.component.html',
  styleUrls: ['./webcam.component.scss']
})
export class WebcamComponent implements OnInit, AfterViewInit {

  canvas: any;

  @ViewChild('p5js') p5js!: ElementRef;


  constructor() { }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(): void {

    console.log( this.p5js );


    const sketch = (s: any ) => {

      // const density = "Ñ@#W$9876543210?!abc;:+=-,._                    ";
      const density = '       .:-i|=+%O#@'

      let video: any;
      let asciiDiv: HTMLDivElement;

      s.setup = () => {
        s.noCanvas();
        video = s.createCapture( p5.prototype.VIDEO );
        video.size(64, 48);
        // asciiDiv = s.createDiv();
        asciiDiv = this.p5js.nativeElement as HTMLDivElement;
      }

      s.draw = () => {
        video.loadPixels();
        let asciiImage = "";
        for (let j = 0; j < video.height; j++) {
          for (let i = 0; i < video.width; i++) {
            const pixelIndex = (i + j * video.width) * 4;
            const r = video.pixels[pixelIndex + 0];
            const g = video.pixels[pixelIndex + 1];
            const b = video.pixels[pixelIndex + 2];
            const avg = (r + g + b) / 3;
            const len = density.length;
            const charIndex = p5.prototype.floor(p5.prototype.map(avg, 0, 255, 0, len));
            const c = density.charAt(charIndex);
            if (c == " ") asciiImage += "&nbsp;";
            else asciiImage += c;
          }
          asciiImage += '<br/>';
        }
        asciiDiv.innerHTML = asciiImage;
      }

    }

    this.canvas = new p5(sketch);


  }

}
